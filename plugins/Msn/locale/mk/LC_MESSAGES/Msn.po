# Translation of StatusNet - Msn to Macedonian (Македонски)
# Exported from translatewiki.net
#
# Author: Bjankuloski06
# --
# This file is distributed under the same license as the StatusNet package.
#
msgid ""
msgstr ""
"Project-Id-Version: StatusNet - Msn\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-09-25 21:42+0000\n"
"PO-Revision-Date: 2011-09-25 21:44:26+0000\n"
"Language-Team: Macedonian <https://translatewiki.net/wiki/Portal:mk>\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POT-Import-Date: 2011-06-05 21:51:08+0000\n"
"X-Generator: MediaWiki 1.19alpha (r98079); Translate extension (2011-09-22)\n"
"X-Translation-Project: translatewiki.net at https://translatewiki.net\n"
"X-Language-Code: mk\n"
"X-Message-Group: #out-statusnet-plugin-msn\n"
"Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;\n"

#. TRANS: MSN bot status message.
msgid "Send me a message to post a notice"
msgstr "Испрати ми порака за да објавам забелешка"

#. TRANS: Server exception thrown when a message to be sent through MSN cannot be added to the database queue.
msgid "Database error inserting queue item."
msgstr "Грешка во базата при вметнувањето на ставка во редот на чекање."

#. TRANS: Display name of the MSN instant messaging service.
msgid "MSN"
msgstr "MSN"

#. TRANS: Exception thrown when configuration for the MSN plugin is incomplete.
msgid "Must specify a user."
msgstr "Мора да наведете корисник."

#. TRANS: Exception thrown when configuration for the MSN plugin is incomplete.
msgid "Must specify a password."
msgstr "Мора да наведете лозинка."

#. TRANS: Exception thrown when configuration for the MSN plugin is incomplete.
msgid "Must specify a nickname."
msgstr "Мора да наведете прекар."

#. TRANS: Plugin description.
msgid ""
"The MSN plugin allows users to send and receive notices over the MSN network."
msgstr ""
"Приклучокот за MSN им овозможува на корисниците да испраќаат и примаат "
"забелешки преку мрежата на MSN."
