# Translation of StatusNet - LdapAuthorization to German (Deutsch)
# Exported from translatewiki.net
#
# Author: Fujnky
# Author: MF-Warburg
# --
# This file is distributed under the same license as the StatusNet package.
#
msgid ""
msgstr ""
"Project-Id-Version: StatusNet - LdapAuthorization\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-09-25 21:42+0000\n"
"PO-Revision-Date: 2011-09-25 21:44:10+0000\n"
"Language-Team: German <https://translatewiki.net/wiki/Portal:de>\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POT-Import-Date: 2011-06-05 21:50:25+0000\n"
"X-Generator: MediaWiki 1.19alpha (r98079); Translate extension (2011-09-22)\n"
"X-Translation-Project: translatewiki.net at https://translatewiki.net\n"
"X-Language-Code: de\n"
"X-Message-Group: #out-statusnet-plugin-ldapauthorization\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. TRANS: Exception thrown when initialising the LDAP Auth plugin fails because of an incorrect configuration.
msgid ""
"provider_name must be set. Use the provider_name from the LDAP "
"Authentication plugin."
msgstr ""
"Der „provider_name“ muss eingestellt werden. Verwende den „provider_name“ "
"aus dem LDAP-Authentifizierungsplugin."

#. TRANS: Exception thrown when initialising the LDAP Auth plugin fails because of an incorrect configuration.
msgid "uniqueMember_attribute must be set."
msgstr "Der Parameter „uniqueMember_attribute“ muss eingestellt werden."

#. TRANS: Plugin description.
msgid ""
"The LDAP Authorization plugin allows for StatusNet to handle authorization "
"through LDAP."
msgstr ""
"Das LDAP-Berechtigungs-Plugin erlaubt es StatusNet, Berechtigungen durch "
"LDAP abzuwickeln."
